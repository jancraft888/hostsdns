![hostsdns logo](./hostsdns.png)

# hostsdns
expose your /etc/hosts to other devices in your network

## usage
```shell
hostsdns -h
```
