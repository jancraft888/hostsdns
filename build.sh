env GOOS=linux GOARCH=arm64 go build -o hostsdns-linux-arm64
env GOOS=linux GOARCH=amd64 go build -o hostsdns-linux-amd64
env GOOS=darwin GOARCH=amd64 go build -o hostsdns-darwin-amd64
env GOOS=darwin GOARCH=arm64 go build -o hostsdns-darwin-arm64
