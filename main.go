package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
  "net"

	"github.com/miekg/dns"
)

var portPtr = flag.Int("port", 53, "the port to listen at")
var recordsPtr = flag.String("records", "/etc/hosts", "the records file")
var ifacePtr = flag.String("iface", "169.254", "the preferred interface address prefix")

var address = "127.0.0.1"
var records = map[string]string{}

func parseQuery(m *dns.Msg) {
	for _, q := range m.Question {
		switch q.Qtype {
		case dns.TypeA:
			log.Printf("Query for %s\n", q.Name)
			ip := records[q.Name]
			if ip != "" {
				rr, err := dns.NewRR(fmt.Sprintf("%s A %s", q.Name, ip))
				if err == nil {
					m.Answer = append(m.Answer, rr)
				}
			}
		}
	}
}

func readRecords() {
  dat, err := os.ReadFile(*recordsPtr)
  if err != nil {
    panic(err)
  }
  lines := strings.Split(string(dat), "\n")

  for _, line := range lines {
    line = strings.Trim(line, " \t")
    line = strings.ReplaceAll(line, "\t", " ")
    if strings.HasPrefix(line, "#") { continue }
    if strings.HasPrefix(line, ":") { continue }
    if len(line) < 1 { continue }

    parts := strings.Split(line, " ")
    
    records[parts[1] + "."] = parts[0]
    if parts[0] == "127.0.0.1" {
      records[parts[1] + "."] = address
    }
  }

  fmt.Println(records)
}

func readAddress() {
  ifaces, err := net.Interfaces()
  if err != nil {
    panic(err)
  }
  for _, i := range ifaces {
    addrs, err := i.Addrs()
    if err != nil {
      panic(err)
    }
    for _, addr := range addrs {
      var ip net.IP
      switch v := addr.(type) {
      case *net.IPNet:
              ip = v.IP
      case *net.IPAddr:
              ip = v.IP
      }
      if strings.HasPrefix(ip.String(), *ifacePtr) {
        fmt.Printf("found address: %s\n", ip.String())
        address = ip.String()
        return
      }
    }
  }
  fmt.Println("warn: could not find an appropiate address")
}

func handleDnsRequest(w dns.ResponseWriter, r *dns.Msg) {
	m := new(dns.Msg)
	m.SetReply(r)
	m.Compress = false

	switch r.Opcode {
	case dns.OpcodeQuery:
		parseQuery(m)
	}

	w.WriteMsg(m)
}

func main() {
	// attach request handler func
	dns.HandleFunc(".", handleDnsRequest)

  flag.Parse()
  readAddress()
  readRecords()

	// start server
	port := *portPtr
	server := &dns.Server{Addr: ":" + strconv.Itoa(port), Net: "udp"}
	log.Printf("Starting at %d\n", port)
	err := server.ListenAndServe()
	defer server.Shutdown()
	if err != nil {
		log.Fatalf("Failed to start server: %s\n ", err.Error())
	}
}

